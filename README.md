## Prerequisites

-   [Node 14](https://nodejs.org/)
-   [Docker](https://docs.docker.com/get-docker/)

## Initial setup

1. `npm install`
2. Copy `default.env` to `.env` and update the sign key (only needed for interactions with testnets/mainnet)

## Running scripts

- `npm run deploy` deploys a new contract (`.env` needs to be updated if you deploy a new contract)
- `npx run mint <amount> <destination>` mints new tokens for an address
- `npx run transfer <tokenId> <amount> <source> <destination>` transfers tokens

import { TezosToolkit } from '@taquito/taquito';
import * as dotenv from 'dotenv';
import { tezosToolkit } from './tezos-toolkit';

export function delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

export function run(what: (tezos: TezosToolkit) => Promise<void>) {
    dotenv.config();
    let tezos = tezosToolkit();
    what(tezos).catch(e => {
        debugger;
        console.error(e);
    });
}

export function getBytes(input: string): string {
	const bytes = [];
	for (let n = 0, l = input.length; n < l; n ++) {
        const hex = Number(input.charCodeAt(n)).toString(16);
        bytes.push(hex);
    }

	return bytes.join('');
}
import { runOperation, tezosToolkit } from './common/tezos-toolkit';
import { run } from './common/utils';
import yargs from 'yargs';

run(async tezos => {
    await yargs(process.argv.slice(2))
        .options({
            sk: { type: 'string', default: process.env.SIGN_KEY! },
        })
        .command(
            '$0 <amount> <destination>',
            'transfer tokens',
            yargs => {
                yargs
                    .positional('amount', {
                        type: 'number',
                        default: 0,
                    })
                    .positional('destination', {
                        type: 'string',
                    });
            },
            async argv => {
                let contract = await tezos.contract.at(process.env.DEPLOYED_CONTRACT!);

                let tokens = [
                    {
                        owner: argv.destination,
                        amount: argv.amount,
                    },
                ];

                await runOperation(() => contract.methods.mint_tokens(tokens).send());

                console.log(`All successful.`);
            }
        )
        .help()
        .parse();
});

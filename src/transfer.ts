import { runOperation, tezosToolkit } from './common/tezos-toolkit';
import { run } from './common/utils';
import yargs from 'yargs';

run(async _ => {
    await yargs(process.argv.slice(2))
        .options({
            sk: { type: 'string', default: process.env.SIGN_KEY! },
        })
        .command(
            '$0 <tokenId> <amount> <source> <destination>',
            'transfer tokens',
            yargs => {
                yargs
                    .positional('tokenId', {
                        type: 'number',
                        default: 0,
                    })
                    .positional('amount', {
                        type: 'number',
                    })
                    .positional('source', {
                        type: 'string',
                    })
                    .positional('destination', {
                        type: 'string',
                    });
            },
            async argv => {
                let tezos = tezosToolkit(argv.sk);
                let contract = await tezos.contract.at(process.env.DEPLOYED_CONTRACT!);

                let tokenId = argv.tokenId;
                let from = argv.source;
                let to = argv.destination;
                let amount = argv.amount;

                let transfers = [
                    {
                        from_: from,
                        txs: [{ to_: to, token_id: tokenId, amount }],
                    },
                ];

                await runOperation(() => contract.methods.transfer(transfers).send());

                console.log(`All successful.`);
            }
        )
        .help()
        .parse();
});
